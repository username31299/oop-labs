/**
* Lab05_01 la chuong trinh lam bai 1 lab 5
* @author Hope
* @version 1.0
* @since 2018-09-30
*/
public class Lab05_01
{
	/** 
	* Day la ham main
	* @param args khong su dung
	* @return ham main khong tra ve gia tri nao ca
	*/
	public static void main(String[] args) 
	{
		System.out.println();
		Fruit _fruit = new Fruit();
		QuaCam _quacam = new QuaCam();
		_quacam.setNgayNhap("19/2/2017");
		_quacam.setHanSuDung("2 thang");
		_quacam.setSoLuong("50 kg");
		_quacam.getInfo();

		QuaTao _quatao = new QuaTao();
		_quatao.setNgayNhap("19/3/2017");
		_quatao.setHanSuDung("3 thang");
		_quatao.setSoLuong("52 kg");
		_quatao.getInfo();

		CamCaoPhong _camcaophong = new CamCaoPhong();
		_camcaophong.setNgayNhap("19/4/2017");
		_camcaophong.setHanSuDung("2 thang");
		_camcaophong.setSoLuong("10 kg");
		_camcaophong.getInfo();

		CamSanh _camsanh = new CamSanh();
		_camsanh.setNgayNhap("19/4/2017");
		_camsanh.setHanSuDung("2 thang");
		_camsanh.setSoLuong("20 kg");
		_camsanh.getInfo();
	}
}

/**
* Fruit la cac thuoc tinh cua Hoa Qua
*/
class Fruit
{
	private String name;
	private String giaBanTheoCan;
	private String nguonGocXuatXu;
	private String ngayNhap;
	private String soLuong;
	private String hanSuDung;
	private String color;

	// Setter
	public void setName(String name)
	{
		this.name = name;
	}
	public void setGiaBanTheoCan(String gbtc) 
	{
		this.giaBanTheoCan = gbtc;
	}
	public void setNguonGocXuatXu(String ngxx) 
	{
		this.nguonGocXuatXu = ngxx;
	}
	public void setNgayNhap(String nn) 
	{
		this.ngayNhap = nn;
	}
	public void setSoLuong(String sl) 
	{
		this.soLuong = sl;
	}
	public void setHanSuDung(String hsd) 
	{
		this.hanSuDung = hsd;
	}
	public void setColor(String color)
	{
		this.color = color;
	}

	// Getter
	public String getName()
	{
		return this.name;
	}
	public String getGiaBanTheoCan()
	{
		return this.giaBanTheoCan;
	}
	public String getNguonGocXuatXu()
	{
		return this.nguonGocXuatXu;
	}
	public String getNgayNhap()
	{
		return this.ngayNhap;
	}
	public String getSoLuong()
	{
		return this.soLuong;
	}
	public String getHanSuDung()
	{
		return this.hanSuDung;
	}
	public String getColor()
	{
		return this.color;
	}

	// ham getInfo in ra cac thong tin
	public void getInfo()
	{
		System.out.println("Name: " + getName());
		System.out.println("Color: " + getColor());
		System.out.println("Made in: " + getNguonGocXuatXu());
		System.out.println("Gia ban theo can: " + getGiaBanTheoCan());
		System.out.println("So luong: " + getSoLuong());
		System.out.println("Ngay nhap: " + getNgayNhap());
		System.out.println("HSD: " + getHanSuDung());

		System.out.println();
	}
}

/**
* QuaCam ke thua thuoc tinh cua Fruit
*/
class QuaCam extends Fruit
{
	public QuaCam()
	{
		setName("Qua Cam");
		setColor("orange");
		setGiaBanTheoCan("50000");
	}
	
}

/**
* QuaTao ke thua thuoc tinh cua Fruit
*/
class QuaTao extends Fruit
{
	public QuaTao()
	{
		setName("Qua Tao");
		setColor("red");
		setGiaBanTheoCan("35000");
	}

}

/**
* CamCaoPhong ke thua thuoc tinh cua QuaCam
*/
class CamCaoPhong extends QuaCam
{
	public CamCaoPhong()
	{
		setName("Cam Cao Phong");
		setColor("orange");
		setNguonGocXuatXu("Cao Phong");
		setGiaBanTheoCan("65000");
	}
}

/**
* CamSanh ke thua thuoc tinh cua QuaCam
*/
class CamSanh extends QuaCam
{
	public CamSanh()
	{
		setName("Cam Sanh");
		setColor("orange");
		setNguonGocXuatXu("Trung Quoc");
		setGiaBanTheoCan("55000");
	}
}
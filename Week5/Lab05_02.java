/**
* Lab05_02 la chuong trinh lam bai 2 lab 5
* @author Hope
* @version 1.0
* @since 2018-09-30
*/
public class Lab05_02
{1
	/** 
	* Day la ham main
	* @param args khong su dung
	* @return ham main khong tra ve gia tri nao ca
	*/
	public static void main(String[] args) 
	{
			Shape _shape = new Shape();
			System.out.println(_shape.toString());

			Circle _circle = new Circle();
			_circle.setRadius(2.0);
			_circle.setColor("green");
			_circle.setFilled(false);
			System.out.println(_circle.toString());

			Rectangle _rectangle = new Rectangle();
			_rectangle.setColor("Blue");
			_rectangle.setFilled(false);
			System.out.println(_rectangle.toString());

			Square _square = new Square();
			System.out.println(_square.toString());
	}
}

/**
* Shape la lop dinh nghia 1 hinh
*/
class Shape
{
	String color = "red";
	boolean filled = true;

	// Constructor
	public Shape() {}

	public Shape(String color, boolean filled)
	{
		this.color = color;
		this.filled = filled;
	}


	// Setter / Getter
	public String getColor()
	{
		return this.color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public boolean isFilled()
	{
		return this.filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
	}

	// In ra thong tin
	public String toString() 
	{
		return
			"__ Shape __\n" +
			"Color: " + this.getColor() + "\n" +
			"Filled: " + this.isFilled() + "\n";
	}
}

/**
* Circle la hinh tron ke thua thuoc tinh cua Shape
*/
class Circle extends Shape
{
	final double Pi = Math.PI;
	double radius = 1.0;

	// Constructor
	public Circle() {}

	public Circle(double radius)
	{
		this.radius = radius;
	}

	public Circle(double radius, String color, boolean filled)
	{
		this.radius = radius;
		this.color = color;
		this.filled = filled;
	}

	// Setter / Getter
	public double getRadius()
	{
		return this.radius;
	}

	public void setRadius(double radius)
	{
		this.radius = radius;
	}

	/**
	* ham getArea cho biet dien tich hinh tron
	* @return dien tich hinh tron
	*/
	public double getArea()
	{
		return 4*Pi*this.radius*this.radius;
	}

	/**
	* ham getPeremeter cho biet chu vi hinh trong
	* @return chu vi hinh tron
	*/
	public double getPerimeter()
	{
		return 2*Pi*this.radius;
	}

	// In ra thong tin
	public String toString() 
	{
		return
			"__ Hinh tron __\n" + 
			"Color: " + this.getColor() + "\n" +
			"Radius: " + this.getRadius() + "\n" +
			"Area: " + this.getArea() + "\n" + 
			"Perimeter: " + this.getPerimeter() + "\n";
	}
}

/**
* Rectangle la hinh chu nhat ke thua thuoc tinh cua Shape
*/
class Rectangle extends Shape
{
	private double width = 1.0;
	private double length = 1.0;

	// Constructor
	public Rectangle() {}

	public Rectangle(double width, double length)
	{
		this.width = width;
		this.length = length;
	}

	public Rectangle(double width, double length,
					 String color, boolean filled)
	{
		this.width = width;
		this.length = length;
		this.color = color;
		this.filled = filled;
	}

	// Setter / Getter
	public double getWidth()
	{
		return this.width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public double getLength()
	{
		return this.length;
	}

	public void setLength(double length)
	{
		this.length = length;
	}

	/**
	* ham getArea cho biet dien tich hinh chu nhat
	* @return dien tich hinh chu nhat
	*/
	public double getArea()
	{
		return this.width*this.length;
	}

	/**
	* ham getPerimeter cho biet chu vi hinh chu nhat
	* @return chu vi hinh chu nhat
	*/
	public double getPerimeter()
	{
		return 2*(this.width+this.length);
	}

	// In ra thong tin
	public String toString() 
	{
		return
			"__ Hinh Chu Nhat __\n" +
			"Color: " + this.getColor() + "\n" +
			"Width: " + this.getWidth() + "\n" +
			"Length: " + this.getLength() + "\n" +
			"Area: " + this.getArea() + "\n" + 
			"Perimeter: " + this.getPerimeter() + "\n";
	}
}

/*
* Square la hinh vuong thua ke thuoc tinh cua Rectangle
*/
class Square extends Rectangle
{
	// Constructor
	public Square() {}
	
	public Square(double side)
	{
		this.width = side;
		this.length = side;
	}

	public Square(double side, String color, boolean filled)
	{
		this.width = side;
		this.length = side;
		this.color = color;
		this.filled = filled;
	}

	// Setter / Getter
	public double getSide()
	{
		return this.width;
	}

	public void setSide(double side)
	{
		this.width = side;
		this.length = side;
	}

	public void setWidth(double side)
	{
		this.width = side;
		this.length = side;
	}

	public void setLength(double side)
	{
		this.length = side;
		this.width = side;
	}

	// In ra thong tin
	public String toString() 
	{
		return
			"__ Hinh Vuong __\n" +
			"Color: " + this.getColor() + "\n" +
			"Side: " + this.getWidth() + "\n" +
			"Area: " + this.getArea() + "\n" + 
			"Perimeter: " + this.getPerimeter() + "\n";
	}
}

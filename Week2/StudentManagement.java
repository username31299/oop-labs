/**
* StudentManegement is one application which manages 
* student's info with maximum is 100 students
* @author Hope
* @version 1.0
* @since 2018-09-05
*/

import java.util.*;

public class StudentManagement
{
	// Create an array of students with maximum is 100
	public static Student[] students = new Student[100];

	// Prepare to create info for 20 students to test
	public static int numOfStudents = 20;
	
	/**
	* This is main function manages students's info
	* @param args not use
	* @return main function dont return anything
	*/
	public static void main(String[] args) 
	{
		/*
		* Create new student with my info
		*/
		Student person_1 = new Student();		
		person_1.setName("Do Thanh Dat");
		person_1.setID("17021231");
		person_1.setGroup("N1");
		person_1.setEmail("thanhdatba12@gmail.com");

		// Call and print name by getName function
		System.out.println(person_1.getName());
		String _info_person_1 = person_1.getInfo();


		/*
		* Create 2 students in group K59CLC
		* and 1 student in group K59CB
		*/
		Student person_2 = new Student();
		person_2.setGroup("K59CLC");
		Student person_3 = new Student();
		person_3.setGroup("K59CLC");
		Student person_4 = new Student();
		person_4.setGroup("K59CB");

		/*
		* Check 2 student person_2 and person_3 
		* are in one group or not
		*/
		if (sameGroup(person_2, person_3))
			System.out.println("person_2 and person_3 are same group");
		else
			System.out.println("person_2 and person_3 are not same group");
		
		/*
		* Check 2 student person_2 and person_4
		* are in one group or not
		*/
		if (sameGroup(person_2, person_4))
			System.out.println("person_2 and person_4 are same group");
		else
			System.out.println("person_2 and person_4 are not same group");

		// Create some Students
		for (int i=0; i<numOfStudents; i++)
		{
			/*
			* Create info of i'th student with Name, ID, Email with number i
			* Group of students are : K59CB, K59CA, K59DA
			* Students index mod 3 == 0 are in group K59CB
			* Students index mod 3 == 1 are in group K59CA
			* Students index mod 3 == 2 are in group K59DA
			*/
			students[i] = new Student("Name "+i, "1702"+i, "EmailClone"+i+"@gmail.com");
			if (i % 3 == 0) students[i].setGroup("K59CB");
			else
			if (i % 3 == 1) students[i].setGroup("K59CA");
			else
			if (i % 3 == 2) students[i].setGroup("K59DA");
			/*	
			// Print info of each students for checking
			String inf = students[i].getInfo();
			System.out.println();
			*/
		}

		/*
		* Sort students by group
		*/
		
		for (int i=0; i<numOfStudents; i++)
		{
			for (int j=i+1; j<numOfStudents; j++)
			{
				if ((students[i].getGroup()).compareTo(students[j].getGroup())>0)
				{
					// Change info between 2 students
					Student tempStudent = new Student(students[i]);
					students[i] = new Student(students[j]);
					students[j] = new Student(tempStudent);
				}
			}
		}
		
		/*
		* Print students sorted by group
		*/
		System.out.println();
		String currentGroup = students[0].getGroup();
		System.out.println(" \" " + currentGroup + " \" ");
		for (int i=0; i<numOfStudents; i++)
		{
			String currentStudentsGroup = students[i].getGroup();
			if (currentGroup == currentStudentsGroup)
				System.out.println( students[i].getName() );
			else
			{
				currentGroup = currentStudentsGroup;
				System.out.println();
				System.out.println(" \" " + currentGroup + " \" ");
				System.out.println( students[i].getName() );
			}
		}

		// Test void removeStudent
		removeStudent("17025");
	}


	/**
	* removeStudent if  a void which remove
	* student from list by id
	* @param id ID of student want to remove
	* @return void have no return 
	*/
	public static void removeStudent(String id)
	{
		// Find student with id 
		int index = -1;
		for (int i=0; i<numOfStudents; i++)
		{
			if (students[i].getID() == id)
			{
				index = i;
				break;
			}
		}
		if (index != -1)
		{
			for (int i=index; i<numOfStudents; i++)
			{
				students[i] = new Student(students[i+1]);
			}
			numOfStudents -= 1;
		}
		System.out.println(" \"Removed Student Has ID: " + id + "\" ");
	}

	

	/** 
	* sameGroup is a function which check 2 students are
	* in the same group or not
	* @param s1 the first student
	* @param s2 the second student
	* @return boolean function return value:
	*			true - if 2 students are in the same group
	*			false - if 2 students are int the different group
	*/
	public static boolean sameGroup(Student s1, Student s2)
	{
		return s1.getGroup().equals(s2.getGroup());
	}
}


/**
* Student is one class which contains info of students
* @param name contain student's name
* @param id contain student's code
* @param group contain student's group
* @param email contain student's email
* @return class Student dont return anything
*/
class Student
	{
		private String name;
		private String id;
		private String group;
		private String email;
		
		/**
		* Constructor with no parameter
		*/
		public Student()
		{
			this.name = "Student";
			this.id = "000";
			this.group = "K59CB";
			this.email = "uet@vnu.edu.vn";
		}

		/**
		* Constructor with parameter name, id and email
		* @param n initialize name
		* @param sid initialize id
		* @param em initialize email
		*/
		public Student(String n, String sid, String em)
		{
			this.name = n;
			this.id = sid;
			this.email = em;
			this.group = "K59CB";
		}

		/**
		* Constructor copy info from student "s" to this student
		* @param s contain info of the student
		* 
		*/
		public Student(Student s)
		{
			this.name = s.name;
			this.id = s.id;
			this.group = s.group;
			this.email = s.email;
		}
		
		/**
		* Function get name of student
		* @return name of student
		*/
		public String getName()
		{
			return name;
		}

		/**
		* Void set name of student
		* @param n name of student want to set
		*/
		public void setName(String n)
		{
			this.name = n;
		}

		/**
		* Function get id of student
		* @return id of student
		*/
		public String getID()
		{
			return id;
		}

		/**
		* Void set id of student
		* @param n id of student want to set
		*/
		public void setID(String n)
		{
			this.id = n;
		}

		/**
		* Function get group of student
		* @return group of student
		*/
		public String getGroup()
		{
			return group;
		}

		/** 
		* Void set group of student
		* @param n group of student want to set
		*/
		public void setGroup(String n)
		{
			this.group = n;
		}

		/**
		* Function get email of student
		* @return email of student
		*/
		public String getEmail()
		{
			return email;
		}

		/**
		* Void set email of student
		* @param n email of student want to set
		*/
		public void setEmail(String n)
		{
			this.email = n;
		}

		/**
		* Function get info of student 
		* and print info 
		* @param printName contain name of student to print
		* @param printID contain id of student to print
		* @param printGroup contain group of student to print
		* @param printEmail contain email of student to print
		* @param info contain info to print
		* @return info print on screen
		*/
		String getInfo()
		{
			String printName = "Name: " + name;
			String printID = "ID: " + id;
			String printGroup = "Group: " + group;
			String printEmail = "Email: " + email;

			String info =	printName 	+ "\n" + 
							printID 	+ "\n" +
							printGroup	+ "\n" + 
							printEmail;
			System.out.println(info);

			return info;
		}
	}

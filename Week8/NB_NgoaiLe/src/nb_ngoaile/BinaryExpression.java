/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Ham BinaryExpression tao ra 2 so
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
public abstract class BinaryExpression extends Expression
{
    // Khai bao truu tuong
    abstract public Expression left();
    abstract public Expression right();
}

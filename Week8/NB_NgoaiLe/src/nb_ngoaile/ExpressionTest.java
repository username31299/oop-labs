/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Ham ExpressionTest de test cac ham
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
public class ExpressionTest
{

    /**
     * Day la ham main
     * @param args the command line arguments\
     * @return khong tra ve gia tri nao ca
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        // Tao so 10
        Expression num10 = new Numeral(10);
        // Binh phuong so 10
        Expression res1 = new Square(num10);
        System.out.println(res1.toString()+"\n");
        // Tao so 1
        Expression num1 = new Numeral(1);
        // Tru 10^2 cho 1
        Expression res2 = new Subtraction(res1,num1);
        System.out.println(res2.toString()+"\n");
        
        // Tao so 2 va so 3
        Expression num2 = new Numeral(2);
        Expression num3 = new Numeral(3);
        // Nhan so 2 voi 3
        Expression res3 = new Multiplication(num2,num3);
        System.out.println(res3.toString()+"\n");
        
        // Cong 10^2 - 1 voi 2*3
        Expression res4 = new Addition(res2,res3);
        System.out.println(res4.toString()+"\n");
        // Binh phuong (10^2 - 1 + 2*3)
        Expression final_result = new Square(res4);
        System.out.println(final_result.toString()+"\n");
        
        // Ket qua cua (10^2 - 1 + 2*3)^2
        System.out.println(final_result.evaluate());
        
        try
        {
            Expression num0 = new Numeral(0);
            Expression num4 = new Numeral(4);
            Expression vd = new Division(num4,num0);
            System.out.println(vd.evaluate());
        }
        catch (ArithmeticException e)
        {
            System.out.println("Loi chia cho 0");
        }
    }
    
}

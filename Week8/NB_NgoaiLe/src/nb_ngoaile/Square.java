/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Ham Square thuc hien phep tinh binh phuong
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
public class Square extends Expression
{
    private Expression expression;
    
    // Constructor
    public Square(Expression ex)
    {
        this.expression = ex;
    }

    @Override
    public String toString()
    {
        return expression.evaluate()+"^2";
    }

    @Override
    public int evaluate()
    {
        return expression.evaluate()*expression.evaluate();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Ham Multiplication thuc hien phep nhan
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
public class Multiplication extends BinaryExpression
{
    private Expression left;
    private Expression right;
    
    // Constructor
    public Multiplication(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public Expression left()
    {
        return this.left;
    }

    @Override
    public Expression right()
    {
        return this.right;
    }

    @Override
    public String toString()
    {
        return left + " * " + right;
    }

    @Override
    public int evaluate()
    {
        return left.evaluate() * right.evaluate();
    }

    
}

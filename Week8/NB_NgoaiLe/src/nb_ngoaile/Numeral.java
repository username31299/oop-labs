/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Ham Numeral tao ra 1 so
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
public class Numeral extends Expression
{
    private int value;
    
    // Constructor
    public Numeral(int val)
    {
        this.value = val;
    }
    
    public Numeral()
    {
        
    }

    // Setter / Getter
    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return ""+value;
    }

    @Override
    public int evaluate()
    {
        return getValue();
    }
    
    
}

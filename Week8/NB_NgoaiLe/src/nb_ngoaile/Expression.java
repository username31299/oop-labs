/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile;

/**
 * Expression tao ra ca the gioi
 * @version 1.0
 * @since 2018-10-23
 * @author Hope
 */
abstract public class Expression
{
    abstract public String toString(); 
    abstract public int evaluate();
    
}

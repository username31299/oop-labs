/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile_2;

import java.io.File;
import java.util.Scanner;

/**
 * Chuong trinh loi file khong tin tai
 * @author Hope
 */
public class FileNotFoundException_control
{
    // Tao ra loi
    public static void control() throws java.io.FileNotFoundException
    {
        throw new java.io.FileNotFoundException("Loi khong tim thay file");
    }
     
    /**
     * Day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
           File f = new File("hello.txt");
           Scanner sc = new Scanner(f);
        }
        catch (java.io.FileNotFoundException e)
        {
            System.out.println(e);
        }
    }
}

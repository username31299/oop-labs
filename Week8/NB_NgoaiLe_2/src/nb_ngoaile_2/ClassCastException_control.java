/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile_2;

/**
 * Chuong trinh loi ep kieu
 * @author Hope
 */
public class ClassCastException_control
{
    // Tao ra loi
    public static void control() throws java.lang.ClassCastException
    {
        throw new java.lang.ClassCastException("Loi ClassCast roi ban oi");
    }
    
    /**
     * Day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        try
        {
            Parent p = new Parent();
            Childrent c = new Childrent();
            c = (Childrent)p;
        }
        catch (java.lang.ClassCastException e)
        {
            System.out.println(e);
        }
    }
}

class Parent
{
    public Parent()
    {
        System.out.println("I am Parent");
    }
}

class Childrent extends Parent
{
    public Childrent()
    {
        //super();
        System.out.println("I am Childrent");
    }
}
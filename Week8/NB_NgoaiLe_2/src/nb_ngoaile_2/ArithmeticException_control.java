/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile_2;

/**
 * Chuong trinh loi chia cho 0
 * @author Hope
 */
public class ArithmeticException_control
{
    // Tao ra loi
    public static void control() throws java.lang.ArithmeticException
    {
        throw new java.lang.ArithmeticException("Loi chia cho 0");
    }
     
    /**
     * Day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            System.out.println(10/0);
        }
        catch (java.lang.ArithmeticException e)
        {
            System.out.println(e);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_ngoaile_2;

/**
 * Chuong trinh loi vuot qua so luong mang
 * @author Hope
 */
public class ArrayIndexOutOfBoundsException_control
{
    // Tao ra loi
    public static void control() throws java.lang.ArrayIndexOutOfBoundsException
    {
        throw new java.lang.ArrayIndexOutOfBoundsException("Vuot qua so luong mang");
    }
     
    /**
     * Day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            int[] a = new int[5];
            System.out.println(a[10]);
        }
        catch (java.lang.ArrayIndexOutOfBoundsException e)
        {
            System.out.println(e);
        }
    }
}

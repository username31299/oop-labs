/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nb_io;

import java.io.IOException;

/**
 * chuong trinh main kiem tra class Utils
 * @author Hope
 * @version 100.20
 * @since 2018-10-30
 */
public class NB_IO
{

    /**
     * day la ham main
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args)
    {
        try
        {
        // TODO code application logic here
            System.out.println(". . . TEST WRITE FILE . . . ");
            Utils.writeContentToFile("alibaba.txt");
            Utils.writeContentToFile("alibaba.txt");
            Utils.writeContentToFile("alibaba.txt");
            Utils.writeContentToFile("alibaba.txt");
            System.out.println("DONE!");

            
           
  
            System.out.println("");
            System.out.println(". . . TEST WRITE FILE KEEP DATA . . .");
 
            Utils.writeContentToFile_keepOld("old_ali.txt");
            Utils.writeContentToFile_keepOld("old_ali.txt");
            Utils.writeContentToFile_keepOld("old_ali.txt");
            System.out.println("DONE!");

            System.out.println("");
            System.out.println(". . .TEST FIND FILE IN FOLDER . . .");
            Utils.findFileByName("src/nb_io","Utils.java");
            Utils.findFileByName("src/nb_io","alibaba.txt");
            System.out.println("DONE!");
            
            System.out.println("");
            System.out.println(". . . TEST READ FILE . . .");
            String data = Utils.readContentFromFile("hell.txt");
            
            System.out.println(data);
            System.out.println();
            System.out.println("DONE!");
        }
        catch (IOException e)
        {
            System.out.println(e);
        }
    }
    
}

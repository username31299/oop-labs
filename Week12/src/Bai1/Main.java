package Bai1;

/**
 * Class main test chuong trinh
 * @author Hope
 * @since 2018-xx-xx
 */
public class Main
{
    /**
     * in ra nhung nguoi khong cuoi
     * @param firstPerson Nguoi dung dau gia pha
     */
    public static void printPersonNotMarried(Person firstPerson)
    {
        if (firstPerson instanceof Child || (firstPerson instanceof Parent && ((Parent) firstPerson).getChildren().isEmpty()))
        {
            System.out.println(firstPerson.getName());
        }
        if (firstPerson instanceof Parent)
        {
            for (Person p : ((Parent) firstPerson).getChildren())
            {
                printPersonNotMarried(p);
            }
        }
    }

    /**
     * In ra nhung Parent co 2 con
     * @param firstPerson Nguoi dung dau gia pha
     */
    public static void printParentsHave2Children(Person firstPerson)
    {
        if (firstPerson instanceof Parent)
        {
            if (((Parent) firstPerson).getChildren().size()==2)
            {
                System.out.println(firstPerson.getName());
            }
            for (Person p:((Parent) firstPerson).getChildren())
            {
                printParentsHave2Children(p);
            }
        }
    }

    /**
     * in ra the he cuoi cung cua gia pha
     * @param firstPerson Nguoi dung dau gia pha
     */
    public static void printLastest(Person firstPerson)
    {
        if (firstPerson instanceof Child)
        {
            System.out.println(firstPerson.getName());
        }
        else if (firstPerson instanceof Parent)
        {
            for (Person p:((Parent) firstPerson).getChildren())
            {
                printLastest(p);
            }
        }
    }

    /**
     * Day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        Parent james = new Parent("James","1-3-9","male");
        james.setMarried(true);

        Person ryan = new Child("Ryan","2-2-3","male");
        ryan.setMarried(false);

        Person kai = new Parent("Kai","2-3-2", "female");
        kai.setMarried(true);

        james.addChild(ryan);
        james.addChild(kai);

        Person con1 = new Parent("Tom","3-2-6", "male");
        Person con2 = new Child("Jerry","3-6-5","male");
        Person con3 = new Parent("Mary","3-3-3","female");
        Person con4 = new Child("Jenny","3-4-5","female");

        Person[] persons = {con1, con2, con3, con4};
        ((Parent) kai).addAllChildren(persons);

        ((Parent) con1).addChild(new Child("Alibaba","4-5-6","male"));

        ((Parent) con3).addChild(new Child("Aladin","4-6-6","female"));
        ((Parent) con3).addChild(new Child("Aladu","4-4-4","male"));
        //((Bai1.Parent) con3).addChild(new Bai1.Child("Aladondon","4-9-0","male"));


        james.show(0);

        System.out.println();
        System.out.println("\nPrinting Persons No Married:");
        printPersonNotMarried(james);

        System.out.println("\nPrinting Parents Have 2 Children:");
        printParentsHave2Children(james);

        System.out.println("\nPrinting Lastest Bai1.Person Of Tree:");
        printLastest(james);
    }
}

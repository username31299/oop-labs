package Bai1;

import java.util.ArrayList;

public class Parent extends Person
{
    private ArrayList<Person> children = new ArrayList<>();

    public Parent(String name, String dateOfBirth, String sex)
    {
        super(name, dateOfBirth, sex);
    }

    public ArrayList<Person> getChildren()
    {
        return children;
    }

    public void addChild(Person p)
    {
        children.add(p);
    }

    public void addAllChildren(Person[] persons)
    {
        for (Person p:persons)
        {
            children.add(p);
        }
    }

    @Override
    public void show(int depth)
    {
        for (int i=0; i<depth; i++)
            System.out.print("..");
        System.out.println(this.getName());

        for (Person p:getChildren())
        {
            p.show(depth+1);
        }
    }
}

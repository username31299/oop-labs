package Bai1;

import java.util.ArrayList;

public abstract class Person
{
    private String name;
    private String dateOfBirth;
    private String sex;
    private boolean married;

    public Person(String name, String dateOfBirth, String sex)
    {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
    }

    // Setter - Getter
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public boolean isMarried()
    {
        return married;
    }

    public void setMarried(boolean married)
    {
        this.married = married;
    }

    public abstract void show(int depth);
}

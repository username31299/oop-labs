package Bai1;

public class Child extends Person
{
    public Child(String name, String dateOfBirth, String sex)
    {
        super(name, dateOfBirth, sex);
    }

    @Override
    public void show(int depth)
    {
        for (int i=0; i<depth; i++)
            System.out.print("..");
        System.out.println(this.getName());
    }
}

package Bai2;

/**
 * Thuat toan sap xep chon
 * @author Hope
 * @since 2018-xx-xx
 */
public class SelectionSort implements Algorithm
{
    /**
     * Dinh nghia ham sorted
     * @param a mang can sap xep
     * @return mang sau khi sap xep
     */
    @Override
    public int[] sorted(int[] a)
    {
        int i, j, min_idx;
        int n = a.length;

        for (i =0; i<n-1; i++)
        {
            min_idx = i;
            for (j=i+1; j<n; j++)
            {
                if (a[j]<a[min_idx])
                    min_idx = j;
            }

            int temp = a[i];
            a[i] = a[min_idx];
            a[min_idx] = temp;
        }
        return a;
    }
}

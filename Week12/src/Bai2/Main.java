package Bai2;

import java.util.Arrays;

/**
 * Class Main test chuong trinh
 * @author Hope
 * @since 2018-xx-xx
 */
public class Main
{
    /**
     * Ham in mang
     * @param a mang can in
     */
    public static void printArray(int[] a)
    {
        for (int i=0; i<a.length; i++)
            System.out.print(a[i]+" ");
        System.out.println();
    }

    /**
     * day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        Algorithm selectionSort = new SelectionSort();
        Algorithm bubbleSort = new BubbleSort();

        int[] a = {1,5,3,7,6,1,2,4,4,8};

        a = selectionSort.sorted(a);
        printArray(a);
        System.out.println();

        int[] b = {1,5,3,7,6,1,2,4,4,8};
        b = bubbleSort.sorted(b);
        printArray(b);
    }
}

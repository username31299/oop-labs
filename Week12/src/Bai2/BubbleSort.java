package Bai2;

/**
 * Thuat toan sap xep noi bot
 * @author Hope
 * @since 2018-xx-xx
 */
public class BubbleSort implements Algorithm
{
    /**
     * Dinh nghia ham sorted
     * @param a mang can sap xep
     * @return mang sau khi sap xep
     */
    @Override
    public int[] sorted(int[] a)
    {
        for (int i=0; i<a.length; i++)
        {
            for (int j=i+1; j<a.length; j++)
            {
                if (a[j]<a[i])
                {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a;
    }
}

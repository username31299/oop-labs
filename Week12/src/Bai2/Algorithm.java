package Bai2;

/**
 * interface dinh nghia ham sort
 * @author Hope
 * @since 2018-xx-xx
 */
public interface Algorithm
{
    public int[] sorted(int[] a);
}

package Bai1;

/**
 * Class Main chay chuong trinh bai 1
 * @author Hope
 * @since 2018-xx-xx
 */
public class Main
{
    /**
     * day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        System.out.println(getFibonacci(3));
    }

    /**
     * ham tinh so fibonacci thu n
     * @param n so thu tu
     * @return so fibonacci thu n
     */
    public static int getFibonacci(int n)
    {
        switch (n)
        {
            case 0:
                return 0;
            case 1:
                return 1;
            default:
                return getFibonacci(n-1)+getFibonacci(n-2);
        }
    }
}

package Bai2;

/**
 * Dinh nghia mot phan so
 *
 * @author Hope
 * @since 2018-xx-xx
 */
public class Fraction
{
    private int numerator; // Tu So
    private int denomirator; // Mau So

    /**
     * Contructor
     *
     * @param numerator   tu so
     * @param denomirator mau so
     */
    public Fraction(int numerator, int denomirator)
    {
        this.numerator = numerator;
        this.denomirator = denomirator;
        this.simplify();
    }

    /**
     * Toi gian phan so
     */
    private void simplify()
    {
        int temp = gcd(this.numerator, this.denomirator);
        this.numerator /= temp;
        this.denomirator /= temp;
    }

    /**
     * Ham tinh uoc chung lon nhat cua 2 so
     *
     * @param a so thu 1
     * @param b so thu 2
     * @return uoc chung lon nhat
     */
    private int gcd(int a, int b)
    {
        while (b > 0)
        {
            int t = a % b;
            a = b;
            b = t;
        }
        return a;
    }

    /**
     * Cong phan so
     *
     * @param f phan so cong them
     */
    public void add(Fraction f)
    {
        int t_numerator = this.numerator * f.getDenomirator() + this.denomirator * f.getNumerator();
        int t_denomirator = this.denomirator * f.getDenomirator();

        this.numerator = t_numerator;
        this.denomirator = t_denomirator;
        simplify();
    }

    /**
     * Tru phan so
     *
     * @param f phan so tru di
     */
    public void subtract(Fraction f)
    {
        int t_numerator = this.numerator * f.getDenomirator() - this.denomirator * f.getNumerator();
        int t_denomirator = this.denomirator * f.getDenomirator();

        this.numerator = t_numerator;
        this.denomirator = t_denomirator;

        simplify();
    }

    /**
     * Nhan phan so
     *
     * @param f phan so nhan vao
     */
    public void multiple(Fraction f)
    {
        this.numerator *= f.getNumerator();
        this.denomirator *= f.getDenomirator();
        simplify();
    }

    /**
     * Chia phan so
     *
     * @param f phan so chia di
     */
    public void divide(Fraction f)
    {
        this.numerator *= f.getDenomirator();
        this.denomirator *= f.getNumerator();
        simplify();
    }

    /**
     * ham so sanh hai phan so
     * @param o phan so can so sanh
     * @return ket qua so sanh
     */
    public boolean equals(Object o)
    {
        return (this.numerator == ((Fraction) o).getNumerator()) &&
                (this.denomirator == ((Fraction) o).getDenomirator());
    }

    /**
     * Ham hien thi phan so
     * @return phan so
     */
    public String toString()
    {
        return this.numerator + "/" + this.denomirator;
    }


    // Setter / Getter
    public int getNumerator()
    {
        return numerator;
    }

    public void setNumerator(int numerator)
    {
        this.numerator = numerator;
    }

    public int getDenomirator()
    {
        return denomirator;
    }

    public void setDenomirator(int denomirator)
    {
        this.denomirator = denomirator;
    }
}

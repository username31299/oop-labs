package Bai2;

/**
 * Class Main chay chuong trinh bai 2
 * @author Hope
 * @since 2018-xx-xx
 */
public class Main
{
    /**
     * day la ham main
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        Fraction f1 = new Fraction(1,2);
        Fraction f2 = new Fraction(3,5);
        Fraction f3 = new Fraction(3,6);

        System.out.println(f1);
        System.out.println(f2);
        System.out.println(f3);
        f1.add(f3);
        System.out.println(f1);
    }
}

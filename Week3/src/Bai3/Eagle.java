package Bai3;

/**
 * Dinh nghia con Dai Bang
 * @author Hope
 * @since 2018-xx-xx
 */
public class Eagle
{
    private String sex;
    private int age;
    private String type;

    // Constructor
    public Eagle(String sex, int age, String type)
    {
        this.sex = sex;
        this.age = age;
        this.type = type;
    }

    // Setter / Getter
    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}

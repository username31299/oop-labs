package Bai3;

/**
 * Dinh nghia 1 quyen sach
 * @author Hope
 * @since 2018-xx-xx
 */
public class Book
{
    private String name;
    private String author;
    private String numberOfPages;

    // Constuctor

    public Book(String name, String author, String numberOfPages)
    {
        this.name = name;
        this.author = author;
        this.numberOfPages = numberOfPages;
    }

    // Setter / Getter

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getNumberOfPages()
    {
        return numberOfPages;
    }

    public void setNumberOfPages(String numberOfPages)
    {
        this.numberOfPages = numberOfPages;
    }
}

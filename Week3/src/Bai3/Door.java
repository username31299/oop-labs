package Bai3;

/**
 * Dinh nghia 1 cai cua
 * @author Hope
 * @since 2018-xx-xx
 */
public class Door
{
    private String color;
    private String type;
    private int width;
    private int height;

    // Constructor

    public Door(String color, String type, int width, int height)
    {
        this.color = color;
        this.type = type;
        this.width = width;
        this.height = height;
    }

    // Setter / Getter

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }
}

package Bai3;

/**
 * Dinh nghia 1 nguoi dan ong
 * @author Hope
 * @since 2018-xx-xx
 */
public class Man
{
    private String name;
    private int age;
    private int length;
    private int height;

    // Constructor

    public Man(String name, int age, int length, int height)
    {
        this.name = name;
        this.age = age;
        this.length = length;
        this.height = height;
    }

    // Setter / Getter

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }
}

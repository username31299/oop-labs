package Bai3;

/**
 * Dinh nghia 1 giao vien
 * @author Hope
 * @since 2018-xx-xx
 */
public class Teacher
{
    private String name;
    private int age;
    private String sex;

    // Constructor
    public Teacher(String name, int age, String sex)
    {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    // Setter / Getter
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }
}

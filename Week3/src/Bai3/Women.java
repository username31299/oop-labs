package Bai3;

/**
 * Dinh nghia 1 co gai =)))))
 * @author Hope
 * @since 2018-xx-xx
 */
public class Women
{
    private String name;
    private int age;
    private int depth;
    private int height;

    // Constructor

    public Women(String name, int age, int depth, int height)
    {
        this.name = name;
        this.age = age;
        this.depth = depth;
        this.height = height;
    }

    // Setter / Getter

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public int getDepth()
    {
        return depth;
    }

    public void setDepth(int depth)
    {
        this.depth = depth;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }
}

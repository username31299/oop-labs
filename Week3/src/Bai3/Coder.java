package Bai3;

/**
 * Dinh nghia 1 thang coder dang ngoi code 10 class sml
 * @author Hope
 * @since 2018-xx-xx
 */
public class Coder
{
    private String name;
    private String language;
    private int age;
    private boolean hasLover;

    // Constructor

    public Coder(String name, String language, int age, boolean hasLover)
    {
        this.name = name;
        this.language = language;
        this.age = age;
        this.hasLover = hasLover;
    }

    // Setter / Getter

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public boolean isHasLover()
    {
        return hasLover;
    }

    public void setHasLover(boolean hasLover)
    {
        this.hasLover = hasLover;
    }
}

package Bai3;

public class File
{
    private String path;
    private String name;
    private int numberOfCharacters;

    // Constructor

    public File(String path, String name)
    {
        this.path = path;
        this.name = name;
    }

    // Setter / Getter

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getNumberOfCharacters()
    {
        return numberOfCharacters;
    }
}

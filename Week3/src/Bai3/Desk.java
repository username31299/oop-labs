package Bai3;

/**
 * Dinh nghia cai ban
 * @author Hope
 * @since 2018-xx-xx
 */
public class Desk
{
    private String type;
    private int numberLegs;

    // Contructor

    public Desk(String type, int numberLegs)
    {
        this.type = type;
        this.numberLegs = numberLegs;
    }

    // Setter / Getter
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getNumberLegs()
    {
        return numberLegs;
    }

    public void setNumberLegs(int numberLegs)
    {
        this.numberLegs = numberLegs;
    }
}

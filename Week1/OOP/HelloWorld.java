/**
* Chuong trinh viet Hello World, Java!
* @author Hope
* @since 2018-xx-xx
* @version 1.0
*/
public class HelloWorld
{
	/**
	* Main
	* @param args khong su dung
	*/
	public static void main(String[] args) 
	{
		System.out.println("Hello World, Java!");
	}
}
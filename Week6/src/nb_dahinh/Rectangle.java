package nb_dahinh;
/**
 * Rectangle la hinh chu nhat
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class Rectangle extends Shape {

    private double width = 1.0;
    private double length = 1.0;

    // Constructor
    public Rectangle() {
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length,
            String color, boolean filled) {
        this.width = width;
        this.length = length;
        super.setColor(color);
        super.setFilled(filled);
    }

    // Setter / Getter
    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    /**
     * ham getArea cho biet dien tich hinh chu nhat
     *
     * @return dien tich hinh chu nhat
     */
    public double getArea() {
        return this.width * this.length;
    }

    /**
     * ham getPerimeter cho biet chu vi hinh chu nhat
     *
     * @return chu vi hinh chu nhat
     */
    public double getPerimeter() {
        return 2 * (this.width + this.length);
    }

    // In ra thong tin
    public String toString() {
        return "__ Hinh Chu Nhat __\n"
                + "Color: " + this.getColor() + "\n"
                + "Width: " + this.getWidth() + "\n"
                + "Length: " + this.getLength() + "\n"
                + "Area: " + this.getArea() + "\n"
                + "Perimeter: " + this.getPerimeter() + "\n";
    }
}

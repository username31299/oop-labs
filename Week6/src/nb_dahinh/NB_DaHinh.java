package nb_dahinh;

/**
 * NB_DaHinh la chuong trinh chua ham main
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class NB_DaHinh {

    /**
     * Day la ham main
     * @param args khong su dung
     * @return ham main khong tra ve gia tri nao ca
     */
    public static void main(String[] args) {
        Diagram m_diagram = new Diagram();
        
        Layer lay_1 = new Layer();
        Layer lay_2 = new Layer();
        Layer lay_3 = new Layer();
        
        Shape a1 = new Rectangle();
        Shape a2 = new Square();
        Shape a3 = new Triangle();
        Shape a4 = new Circle();
        
        lay_1.add(a1);
        lay_1.add(a2);
        lay_1.add(a3);
        lay_1.add(a1);
        lay_1.add(a2);
        lay_1.add(a3);
        
        lay_2.add(a1);
        lay_2.add(a3);
        lay_2.add(a4);
        lay_2.add(a1);
        lay_2.add(a3);
        lay_2.add(a4);
        
        lay_3.add(a2);
        lay_3.add(a3);
        lay_3.add(a4);
        lay_3.add(a2);
        lay_3.add(a3);
        lay_3.add(a4);
        
        m_diagram.add(lay_1);
        m_diagram.add(lay_2);
        m_diagram.add(lay_3);
        
        m_diagram.showDiagram();
        
        
        System.out.println("KET QUA SAU KHI XOA HINH TAM GIAC");
        System.out.print("__Layer 1__");
        lay_1.removeAllTriangle();
        lay_1.showLayer();
        
        System.out.print("__Layer 2__");
        lay_2.removeAllTriangle();
        lay_2.showLayer();
        
        System.out.print("__Layer 3__");
        lay_3.removeAllTriangle();
        lay_3.showLayer();
        
        System.out.println("KET QUA SAU KHI XOA HINH TRON");
        m_diagram.removeAllCircleDiagram();
        m_diagram.showDiagram();
        
    }
    
}

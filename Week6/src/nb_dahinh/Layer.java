package nb_dahinh;
/**
 * Layer la 1 nhanh gom cac hinh
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
import java.util.*;

public class Layer
{
    final int MAX_SHAPE = 20;
    Vector<Shape> all_shape;
    
    // Constructor
    public Layer()
    {
        all_shape = new Vector<Shape>();
    }
    
    // Ham them 1 hinh vao Layer
    public void add(Shape o)
    {
        all_shape.add(o);
    }
    
    // Ham xoa hinh tam giac
    public void removeAllTriangle()
    {
        for (int i=all_shape.size()-1; i>=0; i--)
        {
            if (all_shape.get(i) instanceof Triangle)
                all_shape.remove(all_shape.get(i));
        } 
    }
    
    // Ham xoa hinh tron
    public void removeAllCircleLayer()
    {
        for (int i=all_shape.size()-1; i>=0; i--)
        {
            if (all_shape.get(i) instanceof Circle)
                all_shape.remove(all_shape.get(i));
        }
    }
    
    // Ham in cac layer
    public void showLayer()
    {
        for (Shape i : all_shape)
            System.out.println(i.toString());
        System.out.println();
        System.out.println();
    }
}
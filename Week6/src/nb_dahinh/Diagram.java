package nb_dahinh;
/**
 * Diagram la lop dai dien cho so do duoc ve
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
import java.util.*;

public class Diagram
{
    final int MAX_LAYER = 20;
    Vector<Layer> layers;
    
    public Diagram()
    {
        layers = new Vector<Layer>();
    }
    
    /**
     * Ham them 1 Layer vao vector layers
     * @param x Layer muon them vao
     */
    public void add(Layer x)
    {
        layers.add(x);
    }
    
    // Ham xoa tat ca hinh tron trong diagram
    public void removeAllCircleDiagram()
    {
        for (Layer l : layers)
            l.removeAllCircleLayer();
    }
    
    // Ham in cac doi tuong trong diagram
    public void showDiagram()
    {
        int i=0;
        for (Layer l : layers)
        {
            i++;
            System.out.println("__Digram " + i + "__");
            l.showLayer();
        }
    }
}
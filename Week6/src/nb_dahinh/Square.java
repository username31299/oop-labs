package nb_dahinh;
/**
 * Square la hinh vuong
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class Square extends Rectangle {
    // Constructor

    public Square() {
    }

    public Square(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public Square(double side, String color, boolean filled) {
        super.setWidth(side);
        super.setLength(side);
        super.setColor(color);
        super.setFilled(filled);
    }

    // Setter / Getter
    public double getSide() {
        return super.getWidth();
    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public void setWidth(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public void setLength(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    // In ra thong tin
    public String toString() {
        return "__ Hinh Vuong __\n"
                + "Color: " + this.getColor() + "\n"
                + "Side: " + super.getWidth() + "\n"
                + "Area: " + this.getArea() + "\n"
                + "Perimeter: " + this.getPerimeter() + "\n";
    }
}

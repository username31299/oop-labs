package nb_dahinh;

/**
 * Hinh Luc Giac Deu
 * @author Hope
 * @version 1.0
 * @since 2018-xx-xx
 */
public class Hexagon extends Shape
{
    private double side;
    
    // Constructor
    public Hexagon()
    {
        
    }
    
    public Hexagon(double x, double y, double side)
    {
        super.setLocationX(x);
        super.setLocationY(y);
        this.side = side;
    }

    // Setter / Getter
    public double getSide()
    {
        return side;
    }

    public void setSide(double side)
    {
        this.side = side;
    }
    
    // Ham toString in thong tin cua hinh
    public String toString()
    {
        return "__ Hinh luc giac deu __\n"
                + "Color: " + this.getColor() + "\n"
                + "x: " + this.getLocationX() + "\n"
                + "y: " + this.getLocationY() + "\n"
                + "Side: " + this.getSide() + "\n";
    }
    

    @Override
    public boolean equalsTo(Object o)
    {
        Hexagon h = (Hexagon)o;
        return this.getSide()==h.getSide() &&
                this.getLocationX()==h.getLocationX() &&
                this.getLocationY()==h.getLocationY();
    }
    
}

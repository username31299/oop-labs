package nb_dahinh;
/**
 * Circle la hinh tron
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class Circle extends Shape {

    final double Pi = Math.PI;
    double radius = 1.0;

    // Constructor
    public Circle() {
    }

    public Circle(double x, double y, double radius) {
        super.setLocationX(x);
        super.setLocationY(y);
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        this.radius = radius;
        super.setColor(color);
        super.setFilled(filled);
    }

    // Setter / Getter
    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * ham getArea cho biet dien tich hinh tron
     *
     * @return dien tich hinh tron
     */
    public double getArea() {
        return 4 * Pi * this.radius * this.radius;
    }

    /**
     * ham getPeremeter cho biet chu vi hinh trong
     *
     * @return chu vi hinh tron
     */
    public double getPerimeter() {
        return 2 * Pi * this.radius;
    }

    // In ra thong tin
    public String toString() {
        return "__ Hinh tron __\n"
                + "Color: " + this.getColor() + "\n"
                + "Radius: " + this.getRadius() + "\n"
                + "Area: " + this.getArea() + "\n"
                + "Perimeter: " + this.getPerimeter() + "\n";
    }

    @Override
    public boolean equalsTo(Object o)
    {
        Circle c = (Circle)o;
        return this.getRadius()==c.getRadius() &&
                this.getLocationX()==c.getLocationX() &&
                this.getLocationY()==c.getLocationY();
    }
}

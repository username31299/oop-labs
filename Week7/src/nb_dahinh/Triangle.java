package nb_dahinh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

/**
 * Triangle la hinh tam giac 
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class Triangle extends Shape
{
    private double canh1 = 1;
    private double canh2 = 1;
    private double canh3 = 1;
    
    // Constructor
    public Triangle()
    {
        
    }
    
    public Triangle(double c1, double c2, double c3)
    {
        this.canh1 = c1;
        this.canh2 = c2;
        this.canh3 = c3;
    }
    
    public Triangle(String color, boolean filled)
    {
        super.setColor(color);
        super.setFilled(filled);
    }

    public double getCanh1() {
        return canh1;
    }

    public void setCanh1(double canh1) {
        this.canh1 = canh1;
    }

    public double getCanh2() {
        return canh2;
    }

    public void setCanh2(double canh2) {
        this.canh2 = canh2;
    }

    public double getCanh3() {
        return canh3;
    }

    public void setCanh3(double canh3) {
        this.canh3 = canh3;
    }
    
    // In ra thong tin
    public String toString()
    {
        return "__ Hinh Tam Giac __\n"
                + "Color: " + this.getColor() + "\n"
                + "Canh 1: " + this.getCanh1() + "\n"
                + "Canh 2: " + this.getCanh2() + "\n"
                + "Canh 3: " + this.getCanh3() + "\n";
    }

    @Override
    public boolean equalsTo(Object o)
    {
        Triangle t = (Triangle)o;
        double[] this_canh = {this.getCanh1(), this.getCanh2(), this.getCanh3()};
        double[] t_canh = {t.getCanh1(), t.getCanh2(), t.getCanh3()};
        Arrays.sort(this_canh, 0, this_canh.length);
        Arrays.sort(t_canh, 0, t_canh.length);
        for (int i=0; i<t_canh.length; i++)
        {
            if (this_canh[i]!=t_canh[i])
                return false;
        }
        return this.getLocationX()==t.getLocationX() &&
                this.getLocationY()==t.getLocationY();
    }
}
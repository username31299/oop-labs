package nb_dahinh;

/**
 * NB_DaHinh la chuong trinh chua ham main
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public class Main {

    /**
     * Day la ham main
     * @param args khong su dung
     * @return ham main khong tra ve gia tri nao ca
     */
    public static void main(String[] args) {
        Diagram m_diagram = new Diagram();
        
        Layer lay_1 = new Layer();
        Layer lay_2 = new Layer();
        Layer lay_3 = new Layer();
        
        Shape c0 = new Circle(12,40,5);
        Shape c1 = new Circle(40,12,5);
        Shape c2 = new Circle(1,2,3);
                
        Shape r0 = new Rectangle();
        Shape r1 = new Rectangle();
        Shape r2 = new Rectangle();
        
        Shape s0 = new Square();
        Shape s1 = new Square();
        Shape s2 = new Square();
        
        Shape t0 = new Triangle();
        Shape t1 = new Triangle();
        Shape t2 = new Triangle();
        
        Shape h0 = new Hexagon(1,2,3);
        Shape h1 = new Hexagon(2,3,1);
        Shape h2 = new Hexagon(3,3,3);
        
        lay_1.add(c0);
        lay_1.add(r2);
        lay_1.add(s2);
        lay_1.add(t1);
        lay_1.add(c1);
        lay_1.add(t2);
        
        lay_2.add(h2);
        lay_2.add(h1);
        lay_2.add(h0);
        lay_2.add(h2);
        lay_2.add(h1);
        lay_2.add(h0);
        
        lay_3.add(c2);
        lay_3.add(c2);
        lay_3.add(c2);
        lay_3.add(c2);
        lay_3.add(c1);
        lay_3.add(c1);
        
        m_diagram.add(lay_1);
        m_diagram.add(lay_2);
        m_diagram.add(lay_3);
        
        m_diagram.showDiagram();
        
        
        System.out.println("KET QUA SAU KHI XOA HINH TAM GIAC");
        System.out.print("__Layer 1__");
        lay_1.removeAllTriangle();
        lay_1.showLayer();
        
        System.out.print("__Layer 2__");
        lay_2.removeAllTriangle();
        lay_2.showLayer();
        
        System.out.print("__Layer 3__");
        lay_3.removeAllTriangle();
        lay_3.showLayer();
        System.out.println("__Layer 3 After Delete Same Shape");
        lay_3.deleteSameShape();
        lay_3.showLayer();
        
        System.out.println("__Diagram After Seperate__");
        m_diagram.seperateShapes();
        m_diagram.showDiagram();
        
        /*
        System.out.println("KET QUA SAU KHI XOA HINH TRON");
        m_diagram.removeAllCircleDiagram();
        m_diagram.showDiagram();
        */
        
    }
    
}

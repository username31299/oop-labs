package nb_dahinh;

/**
 * Shape la lop bieu dien 1 hinh
 *
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
public abstract class Shape
{
    private double locationX = 1.0;
    private double locationY = 1.0;
    private boolean filled = true;
    private String color = "red";

    // Constructor
    public Shape()
    {

    }

    public Shape(String color, boolean filled)
    {
        this.color = color;
        this.filled = filled;
    }
    
    public Shape(String color, boolean filled, double x, double y)
    {
        this.color = color;
        this.filled = filled;
        this.locationX = x;
        this.locationY = y;
    }

    // Getter / Setter
    public double getLocationX()
    {
        return locationX;
    }

    public void setLocationX(double locationX)
    {
        this.locationX = locationX;
    }

    public double getLocationY()
    {
        return locationY;
    }

    public void setLocationY(double locationY)
    {
        this.locationY = locationY;
    }

    public boolean isFilled()
    {
        return filled;
    }

    public void setFilled(boolean filled)
    {
        this.filled = filled;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }
    
    // Ham so sanh 2 doi tuong cung kieu
    public abstract boolean equalsTo(Object o);
}

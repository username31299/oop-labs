package nb_dahinh;
/**
 * Diagram la lop dai dien cho so do duoc ve
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
import java.util.*;

public class Diagram
{
    final int MAX_LAYER = 20;
    Vector<Layer> layers;
    
    // Constructor
    public Diagram()
    {
        layers = new Vector<Layer>();
    }
    
    /**
     * Ham them 1 Layer vao vector layers
     * @param x Layer muon them vao
     */
    public void add(Layer x)
    {
        layers.add(x);
    }
    
    // Ham xoa tat ca hinh tron trong diagram
    public void removeAllCircleDiagram()
    {
        for (Layer l : layers)
            l.removeAllCircleLayer();
    }
    
    // Ham chia cac hinh ra tung layer
    public void seperateShapes()
    {
        Layer layer_circle = new Layer();
        Layer layer_rectangle = new Layer();
        Layer layer_square = new Layer();
        Layer layer_triangle = new Layer();
        Layer layer_hexagon = new Layer();
        
        for (Layer l:layers)
        {
            for (Shape s:l.getAll_shape())
            {
                if (s instanceof Circle) layer_circle.add(s);
                else if (s instanceof Rectangle && !(s instanceof Square)) layer_rectangle.add(s);
                else if (s instanceof Square) layer_square.add(s);
                else if (s instanceof Triangle) layer_triangle.add(s);
                else if (s instanceof Hexagon) layer_hexagon.add(s);
            }
        }
        
        layers.clear();
        if (layer_circle.getAll_shape().size()>0) layers.add(layer_circle);
        if (layer_rectangle.getAll_shape().size()>0)layers.add(layer_rectangle);
        if (layer_square.getAll_shape().size()>0)layers.add(layer_square);
        if (layer_triangle.getAll_shape().size()>0)layers.add(layer_triangle);
        if (layer_hexagon.getAll_shape().size()>0)layers.add(layer_hexagon);
    }
    
    // Ham in cac doi tuong trong diagram
    public void showDiagram()
    {
        int i=0;
        for (Layer l : layers)
            if (l.isVisible())
            {
                i++;
                System.out.println("__Digram " + i + "__");
                l.showLayer();
            }
    }
}
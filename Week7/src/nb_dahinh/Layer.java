package nb_dahinh;

/**
 * Layer la 1 nhanh gom cac hinh
 *
 * @author Hope
 * @version 1.0
 * @since 2018 - 10 - 09
 */
import java.util.*;

public class Layer
{

    final int MAX_SHAPE = 20;
    Vector<Shape> all_shape;
    private boolean visible = true;

    // Constructor
    public Layer()
    {
        all_shape = new Vector<Shape>();
    }

    // Setter / Getter
    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public Vector<Shape> getAll_shape()
    {
        return all_shape;
    }

    public void setAll_shape(Vector<Shape> all_shape)
    {
        this.all_shape = all_shape;
    }
    

    // them 1 hinh vao danh sach
    public void add(Shape o)
    {
        all_shape.add(o);
    }

    // Ham xoa tat ca hinh tam giac
    public void removeAllTriangle()
    {
        for (int i = all_shape.size() - 1; i >= 0; i--)
        {
            if (all_shape.get(i) instanceof Triangle)
            {
                all_shape.remove(all_shape.get(i));
            }
        }
    }

    // Ham xoa tat ca hinh tron
    public void removeAllCircleLayer()
    {
        for (int i = all_shape.size() - 1; i >= 0; i--)
        {
            if (all_shape.get(i) instanceof Circle)
            {
                all_shape.remove(all_shape.get(i));
            }
        }
    }

    // Ham in layer
    public void showLayer()
    {
        for (Shape i : all_shape)
        {
            System.out.println(i.toString());
        }
        System.out.println();
        System.out.println();
    }

    // ham xoa cac hinh giong nhau
    public void deleteSameShape()
    {
        for (int i = 0; i < all_shape.size(); i++)
        {
            for (int j = all_shape.size() - 1; j > i; j--)
            {
                if (all_shape.get(i).getClass().equals(all_shape.get(j).getClass()))
                {
                    if (all_shape.get(i).equalsTo(all_shape.get(j)))
                    {
                        all_shape.remove(j);
                    }
                }
            }
        }
    }

}

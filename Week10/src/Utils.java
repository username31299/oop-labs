/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/**
 * Chuong trinh doc viet thao tac voi file
 *
 * @author Hope
 * @version 1.100
 * @since 2018-10-30
 */
public class Utils
{

    /**
     * doc du lieu tu file
     *
     * @param path duong dan toi file
     * @return du lieu trong file
     */
    public static String readContentFromFile(String path)
    {
        String res = "";
        try
        {
            File file = new File(path);
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine())
            {
                String s = sc.nextLine();
                res = res + s + "\n";
            }
        }
        catch (java.io.FileNotFoundException e)
        {
            System.out.printf("Cannot find file %s !\n", path);
        }
        return res;
    }

    /**
     * viet du lieu ra file xoa du lieu cu trong file
     *
     * @param path duong dan toi file
     * @throws IOException
     */
    public static void writeContentToFile(String path) throws IOException
    {
        Writer w = new FileWriter(path);
        w.write("Hello, Iam the fastest man alive!!!\n");
        w.close();
    }

    /**
     * viet du lieu ra file giu nguyen du lieu cu trong file
     *
     * @param path duong dan toi file
     * @throws IOException
     */
    public static void writeContentToFile_keepOld(String path) throws IOException
    {
        File file = new File(path);
        try (FileWriter fr = new FileWriter(file, true);
                BufferedWriter br = new BufferedWriter(fr);
                PrintWriter pr = new PrintWriter(br);)
        {
            pr.println("Ahihi Iam the fastest man alive!!!!");
        }

    }

    /**
     * tim file trong folder
     *
     * @param folderPath duong dan toi folder
     * @param fileName ten file
     * @return file neu ton tai trong folder, NULL neu khong ton tai
     */
    public static File findFileByName(String folderPath, String fileName)
    {
        File folder = new File(folderPath);
        if (folder.isDirectory())
        {
            for (File f : folder.listFiles())
            {
                if (f.getName().equals(fileName))
                {
                    System.out.printf("File %s Exsisted !\n", fileName);
                    return f;
                }
            }
        }
        System.out.printf("File %s Not Found!\n", fileName);
        return null;
    }
}

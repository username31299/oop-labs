import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{
    /**
     * Ham lay ra tat ca functions static
     * @param path duong dan den file
     * @return danh sach cac functions static
     * @throws IOException
     */
    public static List<String> getAllFunctions(File path) throws IOException
    {
        System.out.println("Start Getting Functions . . .");
        List<String> res = new ArrayList<String>();
        if (! path.isFile())
        {
            System.out.println("This file is not file!!!!!!|");
        }
        else
        {
            Scanner sc = new Scanner(path);
            int comment = 0;
            while (sc.hasNextLine())
            {
                String line = sc.nextLine();
                String str = null;
                if (line.contains("static"))
                {
                    str = line;
                    if (line.contains("//") && line.indexOf("//") < line.indexOf("static"))
                    {
                        comment = 1;
                    }
                    if (line.contains("/*") && line.indexOf("/*") < line.indexOf("static") || comment > 0)
                    {
                        // Do nothing
                        comment = 2;
                        while (comment==2)
                        {
                            if (sc.hasNextLine())
                            {
                                String newLine = sc.nextLine();
                                for (int i = 0; i < newLine.length() - 1; i++)
                                {
                                    if (comment == 2 && newLine.substring(i, i + 1) == "*/")
                                    {
                                        comment = 0;
                                    }
                                    else if (comment == 0 && newLine.substring(i, i + 1) == "/*")
                                    {
                                        comment = 2;
                                    }
                                }
                            }
                            else
                                break;
                        }
                    }
                    if (comment == 0)
                    {
                        int countBrace = -1;
                        while (countBrace!=0)
                        {
                            String nextLine = sc.nextLine();
                            str = str + "\n" + nextLine;
                            for (Character c : nextLine.toCharArray())
                            {
                                if (c == '{')
                                {
                                    if (countBrace==-1)
                                    {
                                        countBrace=1;
                                    }
                                    else
                                        countBrace++;
                                }
                                else if (c == '}')
                                    countBrace--;
                                if (countBrace == 0) break;
                            }
                        }
                    }
                    else if (comment == 1)
                    {
                        comment = 0;
                    }
                }
                if (str!=null)
                {
                    res.add(str);
                    str = null;
                }
            }
        }
        return res;
    }

    /**
     * tim cac function co ten cho truoc
     * @param path duong dan den file
     * @param name ten cho truoc
     * @return function co ten nhu da cho, null neu khong co
     * @throws IOException
     */
    public static String findFunctionByName(File path, String name) throws IOException
    {
        List<String> listFuncs = getAllFunctions(path);

        for (String l : listFuncs)
        {
            if (getFunctionName(l).equals(name))
            {
                return l;
            }
        }
        return "Cannot find function!!!!!!!";
    }

    /**
     * lay ten function
     * @param s dong lenh chua ten function
     * @return ten function
     */
    public static String getFunctionName(String s)
    {
        String firstLine = s.substring(0,s.indexOf("\n"));

        int brace = firstLine.indexOf("(");
        String name = firstLine.substring(firstLine.indexOf("("),firstLine.indexOf(")")+1);
        for (int i=brace-1; firstLine.charAt(i)!=' '; i--)
        {
            name = firstLine.charAt(i)+name;
        }

        String tempRes = name.substring(0,name.indexOf('(')+1);

        String stringInBrace = name.substring(name.indexOf('(')+1,name.indexOf(')')-1);
        String[] splitComma = stringInBrace.split(",");
        for (int i=0; i<splitComma.length; i++)
        {
            tempRes = tempRes + splitComma[i].trim().split(" ")[0] + ",";
        }

        String res = tempRes.substring(0,tempRes.length()-1)+")";
        return res;
    }

    /**
     * day la ham main
     * @param args kho0ng su dung
     * @throws IOException
     */
    public static void main(String[] args) throws IOException
    {
        File file = new File("src/Utils.java");
        List<String> stringList = getAllFunctions(file);
        //printListString(stringList);


        String funcName = findFunctionByName(file, "findFileByName(String,String)");
        System.out.println(funcName);

    }

    /**
     * in ra cac phan tu trong danh sach
     * @param stringList danh sach
     */
    public static void printListString(List<String> stringList)
    {
        for (String s : stringList)
        {
            System.out.println(s);
        }
    }
}

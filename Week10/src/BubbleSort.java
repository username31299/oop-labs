import java.util.Random;

/**
 * thuat toan sap xep noi bot
 * @author Hope
 * @since 2018-xx-xx
 */
public class BubbleSort
{
    /**
     * ham sap xep noi boi
     * @param arr mang can sap xep
     */
    public static void BBSort(int[] arr)
    {
        for (int i=0; i<arr.length; i++)
        {
            for (int j=i+1; j<arr.length; j++)
            {
                if (arr[j]<arr[i])
                {
                    int t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                }
            }
        }
    }

    /**
     * day la ham main test ham BBSort
     * @param args khong su dung
     */
    public static void main(String[] args)
    {
        int n = 1000;
        int[] a = new int[n];
        for (int i=0; i<n; i++)
        {
            Random rand = new Random();
            a[i] = rand.nextInt(10000);
        }

        for (int i=0; i<n; i++)
            System.out.printf("%d ",a[i]);
        System.out.println();

        BBSort(a);

        for (int i=0; i<n; i++)
            System.out.printf("%d ",a[i]);
        System.out.println();
    }
}

/**
* Lab04 la chuong trinh lam BT tuan 4
* @author Hope
* @version 1.0
* @since 2018-09-24
*/
public class Lab04
{
	/**
	* Day la ham main
	* @param args khong su dung
	* @return ham main khong tra ve gia tri nao ca
	*/
	public static void main(String[] args) 
	{
		
	}

	/**
	* Ham getGreater lay ra so lon hon trong 2 so
	* @param a so thu nhat
	* @param b so thu hai
	* @return so lon hon tron 2 so
	*/
	public int getGreater(int a, int b)
	{
		if (a>b)
			return a;
		return b;
	}

	/**
	* Ham getLowestFactor lay ra so be nhat trong 
	* mot mang so nguyen
	* @param ar mang so nguyen
	* @return so be nhat trong mang so nguyen
	*/
	public int getLowestFactor(int[] ar)
	{
		int res = ar[0];
		for (int i=1; i<ar.length; i++)
			if (ar[i]<res)
				res = ar[i];
		return res;
	}

	/**
	* Ham BMI cho biet tinh trang suc khoe
	* cua 1 nguoi 
	* @param weight can nang
	* @param height chieu cao
	* @return khong tra ve gia tri nao ca
	*/
	public String BMI(double weight, double height)
	{
		double bmi = weight / height / height;
		String res;
		if (bmi<18.5)
			res = "Thieu can";
		else
		if (bmi<=22.99)
			res = "Binh thuong";
		else
		if (bmi<=24.99)
			res = "Thua can";
		else
			res = "Beo phi";
		return res;
	}
}
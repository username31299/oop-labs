import static org.junit.Assert.assertEquals;
import java.util.*;
import org.junit.Test;

// javac -cp junit-4.12.jar;. Lab04JUnitTest.java
// java -cp junit-4.12.jar;hamcrest-core-1.3.jar;. org.junit.runner.JUnitCore Lab04JUnitTest

public class Lab04JUnitTest
{
	Lab04 lab04 = new Lab04();

	/*
	* Test ham getGreater
	*/
	@Test
	public void testGetGreater1()
	{
		int res = lab04.getGreater(1,3);
		System.out.println(res);
		assertEquals(3,res);
	}

	@Test
	public void testGetGreater2()
	{
		int res = lab04.getGreater(2,-16);
		System.out.println(res);
		assertEquals(2,res);
	}

	@Test
	public void testGetGreater3()
	{
		int res = lab04.getGreater(10,30);
		System.out.println(res);
		assertEquals(30,res);
	}

	@Test
	public void testGetGreater4()
	{
		int res = lab04.getGreater(51,302);
		System.out.println(res);
		assertEquals(302,res);
	}

	@Test
	public void testGetGreater5()
	{
		int res = lab04.getGreater(21,13);
		System.out.println(res);
		assertEquals(21,res);
	}

	/*
	* Test ham getLowestFactor
	*/
	@Test
	public void testGetLowestFactor1()
	{
		int[] a = {1,2,3,4,5,6,7,8,9};
		int res = lab04.getLowestFactor(a);
		System.out.println(res);
		assertEquals(res,1);
	}
	@Test
	public void testGetLowestFactor2()
	{
		int[] a = {11,23,34,45,5,68,77,80,91};
		int res = lab04.getLowestFactor(a);
		System.out.println(res);
		assertEquals(res,5);
	}
	@Test
	public void testGetLowestFactor3()
	{
		int[] a = {14,24,34,44,54,64,74,84,94};
		int res = lab04.getLowestFactor(a);
		System.out.println(res);
		assertEquals(res,14);
	}
	@Test
	public void testGetLowestFactor4()
	{
		int[] a = {1,51,48,61,15,15413,1515,-12};
		int res = lab04.getLowestFactor(a);
		System.out.println(res);
		assertEquals(res,-12);
	}
	@Test
	public void testGetLowestFactor5()
	{
		int[] a = {-1, 3, 5, -3, 12, 0};
		int res = lab04.getLowestFactor(a);
		System.out.println(res);
		assertEquals(res,-3);
	}

	/*
	* Test ham BMI
	*/
	@Test
	public void testBMI1()
	{
		String res = lab04.BMI(60,1.72);
		System.out.println(res);
		assertEquals(res,"Binh thuong");
	}
	@Test
	public void testBMI2()
	{
		String res = lab04.BMI(62,1.60);
		System.out.println(res);
		assertEquals(res,"Thua can");
	}
	@Test
	public void testBMI3()
	{
		String res = lab04.BMI(50,1.50);
		System.out.println(res);
		assertEquals(res,"Binh thuong");
	}
	@Test
	public void testBMI4()
	{
		String res = lab04.BMI(84,1.75);
		System.out.println(res);
		assertEquals(res,"Beo phi");
	}
	@Test
	public void testBMI5()
	{
		String res = lab04.BMI(68,1.78);
		System.out.println(res);
		assertEquals(res,"Binh thuong");
	}
}
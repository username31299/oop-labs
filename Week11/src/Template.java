import java.util.ArrayList;

/**
* CLass Template tao ra cac ham theo yeu cau de bai
* @author Hope
* @since 2018-xx-xx
*/
public class Template
{
    /**
    * Ham sort
    * @param a mang co kieu T
    */
    public <T extends Comparable> T[] sorted(T[] a)
    {
        for (int i = 0; i < a.length; i++)
            for (int j = i + 1; j < a.length; j++)
            {
                if (a[i].compareTo(a[j]) > 0)
                {
                    T temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        return a;
    }

    /**
    * Ham tim phan tu lon nhat trong mang
    * @param arrayList mang kieu T
    */
    public <T extends Comparable> T maxElement(ArrayList<T> arrayList)
    {
        if (arrayList.isEmpty())
            return null;
        T emax = arrayList.get(0);
        for (T i:arrayList)
        {
            if (i.compareTo(emax)>0)
            {
                emax = i;
            }
        }
        return emax;
    }
}

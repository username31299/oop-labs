import java.util.ArrayList;
import java.util.Collection;
/**
* Class Main chay chuong trinh
* @author Hope
* @version 1.0
* @since 2018-xx-xx
*/
public class Main
{
	/**
	* day la ham main
	* @param args khong su dung
	*/
    public static void main(String[] args)
    {
        Template tp = new Template();

        Integer[] a = {1, 4, 1, 4, 5, 2};

        a = tp.sorted(a);

        for (int i=0; i<a.length; i++)
            System.out.print(a[i]+" ");

        ArrayList<Integer> al = new ArrayList<>();
        al.add(1);
        al.add(4);
        al.add(1);
        al.add(4);
        al.add(5);
        al.add(2);

        System.out.println();
        System.out.println("MAX: " + tp.maxElement(al));
    }
}
